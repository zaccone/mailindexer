package main

import (
	"github.com/blevesearch/bleve"
	"log"
)

func getOrCreateIndex(path string) (*bleve.Index, bool) {
	isNew := false
	index, err := bleve.Open(path)
	if err == bleve.ErrorIndexPathDoesNotExist {
		log.Printf("Creating new index...")
		isNew = true
		// create a mapping
		indexMapping := buildIndexMapping()
		index, err = bleve.New(path, indexMapping)
		if err != nil {
			log.Fatal(err)
		}
	} else if err == nil {
		log.Printf("Opening existing index...")
	} else {
		log.Fatal(err)
	}
	return &index, isNew
}

func AddEmail(index *bleve.Index, email MailDir) {

}
