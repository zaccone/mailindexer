package main

import (
	"flag"
	"log"
	"os"
)

/* Setup acceptable flags */

var indexPath = flag.String("index", "db.idx",
	"Path with applications index")
var maildir = flag.String("maildir", "",
	"Directory with e-mails to be indexed")
var query = flag.String("query", "", "query to search")

/*
Parse input parameters and combinations of parameters (sometimes some values
need other input to be provided
*/
func parse() {
	if parsed := flag.Parsed(); parsed == true {
		return
	}

	flag.Parse()

	if *maildir == "" {
		log.Fatal("Specify maildir path with your emails")
	}

}

func main() {
	log.Printf("Starting %s", os.Args[0])
	defer log.Printf("%s exiting\n", os.Args[0])

	parse()

	index, isNew := getOrCreateIndex(*indexPath)
	if isNew == true {
		count := ReadEmails(*maildir, index)
		log.Printf("Read %d e-mails\n", count)
	}

	// go watchForEmails(maildir, index)

}
