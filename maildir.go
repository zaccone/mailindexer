package main

import (
	"io/ioutil"
	"log"
	"net/mail"
	"os"

	"github.com/blevesearch/bleve"
)

type MailDir struct {
	name  string
	email *mail.Message
}

func ReadEmails(maildir string, index *bleve.Index) int {
	log.Printf("Parsing files from directory %s\n", maildir)
	var counter int = 0
	dirEntries, err := ioutil.ReadDir(maildir)
	if err != nil {
		log.Fatal(err)
	}

	for _, dirEntry := range dirEntries {
		dirEntryPath := maildir + string(os.PathSeparator) + dirEntry.Name()
		if dirEntry.IsDir() {
			counter += ReadEmails(dirEntryPath, index)
		} else {
			if email, err := readMaildirEmail(dirEntryPath); err != nil {
				log.Println(err)
			} else {
				counter++
				AddEmail(index, MailDir{dirEntryPath, email})
			}
		}
	}

	return counter
}

func readMaildirEmail(path string) (*mail.Message, error) {

	var file *os.File
	var err error

	if file, err = os.Open(path); err != nil {
		return nil, err
	}
	defer file.Close()

	if email, err := mail.ReadMessage(file); err != nil {
		return nil, err
	} else {
		return email, nil
	}

}
